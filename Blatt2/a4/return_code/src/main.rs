use std::env;
use std::process::exit;

fn main() {
    let args: Vec<String> = env::args().collect();
    exit(args[1].parse::<i32>().unwrap());
}
