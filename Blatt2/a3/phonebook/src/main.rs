// hhoegl, 2021-10-14

use std::io;
use std::io::Write; // write_all(), flush()

#[derive(Debug)]
struct Cmd {
    args: Vec<String>,
}

// funktioni kopiert aus input_loop.rs
fn input(prompt: &[u8]) -> Cmd {
    let stdout = io::stdout();
    let mut handle = stdout.lock();
    let mut cmd = String::new();
    loop {
        let e = handle.write_all(prompt); // -> Result<(), std::io::Error>
        match e {
            Ok(_) => (),
            Err(err) => panic!("error {:?}", err.kind()),
        }
        let e = handle.flush(); // -> Result<(), std::io::Error>
        match e {
            Ok(_) => (),
            Err(err) => panic!("error {:?}", err.kind()),
        }
        cmd.clear();
        io::stdin()
            .read_line(&mut cmd)
            .expect("Failed to read line");
        let v: Vec<&str> = cmd.trim().split(' ').collect();
        match v[0] {
            "" => continue, // just newline
            _ => {
                let mut v2: Vec<String> = vec![];
                for e in v {
                    v2.push(e.to_string());
                }
                return Cmd { args: v2 };
            }
        }
    }
}

fn main() {
    // let mut c: Cmd;

    let mut entries: Vec<(String, String)> = Vec::new();

    loop {
        let c = input(b"Phonebook> ");
        let command = c.args[0].to_string();
        if command == "." {
            break;
        }
        let len = c.args.len();
        if command == "!" {
            if len != 3 {
                println!("need 2 arguments");
                continue;
            }
            let name = c.args[1].to_string();
            let phone = c.args[2].to_string();

            entries.push((name, phone));
            continue;
        }
        if command == "?" {
            if len == 1 {
                for (name, number) in &entries {
                    println!("{name}: {number}");
                }
                continue;
            }
            let name_expected = c.args[1].to_string();
            for (name, number) in &entries {
                if &name_expected != name {
                    continue;
                }
                println!("{name}: {number}");
            }
            continue;
        }
    }
}
