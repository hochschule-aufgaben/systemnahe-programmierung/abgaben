use std::io;

fn acronym(input: &str) -> String {
    let mut acronym = String::new();
    let words = input.split(' ');
    for word_space in words {
        for word_hypen in word_space.split('-') {
            match word_hypen {
                "" => (),
                _ => acronym.push(word_hypen.to_uppercase().chars().next().expect("failed to split up input"))
            };
        }
    }
    acronym
}

fn main() {
    loop {
        let mut guess = String::new();

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        println!("Acronym: {}", acronym(&guess));
    }
}
