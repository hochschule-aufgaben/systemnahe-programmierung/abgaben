use rand::Rng;
use rand::distributions::Alphanumeric;

enum Multitype {
    I32(i32),
    F32(f32),
    STRING(String)
}

fn shuffle(arg: &mut Multitype){
    match arg {
        Multitype::I32(value) => {
            let mut rng = rand::thread_rng();
            *value = rng.gen();
        },
        Multitype::F32(value) => {
            let mut rng = rand::thread_rng();
            *value = rng.gen();
        },
        Multitype::STRING(value) => {
            *value = rand::thread_rng()
                .sample_iter(&Alphanumeric)
                .take(7)
                .map(char::from)
                .collect();
        }
    }
}

fn main() {
    let mut arg_i = Multitype::I32(0);
    let mut arg_f = Multitype::F32(0.0);
    let mut arg_s = Multitype::STRING("".to_string());

    shuffle(&mut arg_i);
    shuffle(&mut arg_f);
    shuffle(&mut arg_s);    

    if let Multitype::I32(arg) = arg_i {
        println!("Result: {}", arg);
    }
    if let Multitype::F32(arg) = arg_f {
        println!("Result: {}", arg);
    }
    if let Multitype::STRING(arg) = arg_s {
        println!("Result: {}", arg);
    }
}
