# installation 
Installing Rust on any Unix-like OS is as easy as running the following command in your terminal:
```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

# Cargo 
Cargo is a versatile tool that not only acts as a package manager similar to Python's pip and JavaScript's npm but also functions as a build tool for Rust. The following are some useful cargo commands:

- _cargo build_ -> compile rust project and generate executable binary

- _cargo run_ -> compile rust project and run the newly generated executable 

- _cargo test_ -> used for running unit test for Rust project

## Crates vs Modules
Rust has two distinct terms that relate to the module system: _‘crate’_ and _‘module’_.

- _Crate_ -> Rust's compilation unit and a package of code. It can be standalone executables, like a program, or they can be libraries that other Rust code can depend on.

- _Module_ -> A module is a way to organize and encapsulate code within a crate. Think of it as "partitions" of a carte.

reference: https://web.mit.edu/rust-lang_v1.25/arch/amd64_ubuntu1404/share/doc/rust/html/book/first-edition/crates-and-modules.html

## using Crate/Module

### Module
To import an item from a module we use the *"use"* keyword to bring that item into your current scope:
```
// Bring a specific item (for example: a function) from a module into current scope
use some_module::some_function;
```

### Crate
The keyword *extern* is used to bring a crate into a scope. Then similar to the example above, we use the *"use"* keyword to import module(s) from a crate.

```
// Bring a module from a crate into scope
extern crate some_crate;
use some_crate::some_module;
```

### including Crate in Rust project
To include a specific Crate to your Rust project, simply get into the Rust configuration file *Cargo.toml* and specify the name and version of the Crate on the dependecies section like so:

```
[package]
name = "hello-rust"
version = "0.1.0"
edition = "2021"

// specify crate name and version under "dependencies"
[dependencies]
your_crate's_name = "0.10"

```
Then simply run:
```
cargo build
```
at the same directory as Cargo.toml, this will install the Crate you specified in Cargo.toml.

# Ferris-says
Upon successfully executing the example project "Ferris-says" as stated on the website, you'll be greeted by Ferris the crab, the beloved mascot of the Rust programming language!

```
< Hello fellow Rustaceans! >
 --------------------------
        \
         \
            _~^~^~_
        \) /  o o  \ (/
          '_   -   _'
          / '-----' \
```