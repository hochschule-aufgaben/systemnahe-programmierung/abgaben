use std::io;

fn main() {
    loop {
        println!("Please input your start number: ");

        let mut number = String::new();

        io::stdin()
            .read_line(&mut number)
            .expect("Failed to read line");

        let number: u32 = match number.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        println!();
        next_collatz(number);
        println!();
    }
}

fn next_collatz(number: u32){
    if number == 1 {
        print!("{number}, [4, 2, 1,]*");
        return;
    }
    print!("{number}, ");
    if (number & 1) == 0 {
        next_collatz(number / 2);
    } else {
        next_collatz(number * 3 + 1);
    }
}