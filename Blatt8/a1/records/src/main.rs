use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::io::Seek;
use std::io::SeekFrom;
use std::mem::transmute;

#[derive(Debug)]
struct Person (
    String,
    String,
    String,
    u32
);

// kopiert von https://stackoverflow.com/questions/42066381/how-to-get-a-str-from-a-nul-terminated-byte-slice-if-the-nul-terminator-isnt
pub unsafe fn str_from_u8_nul_utf8_unchecked(utf8_src: &[u8]) -> String {
    let nul_range_end = utf8_src.iter()
        .position(|&c| c == b'\0')
        .unwrap_or(utf8_src.len()); // default to length if no `\0` present
    ::std::str::from_utf8_unchecked(&utf8_src[0..nul_range_end]).to_string()
}

fn file_get_string(file: &mut File, len: usize) -> String{
    let mut data = vec![0; len];

    file.read(&mut data).expect("Error reading file");

    unsafe {
        str_from_u8_nul_utf8_unchecked(&data)
    }
}

fn file_get_int(file: &mut File) -> u32 {
    let mut data: [u8; 4] = [0; 4];

    file.read(&mut data).expect("Error reading file");

    u32::from_le_bytes(data)
}

fn read_person(file: &mut File) -> Person {
    Person(
        file_get_string(file, 40),
        file_get_string(file, 40),
        file_get_string(file, 240),
        file_get_int(file)
    )
}

fn file_write_string(file: &mut File, string: &str, length: usize){
    file.write(string.as_bytes()).expect("Error writing file");
    file.write(&vec![0u8; length - string.len()]).expect("Error writing file");
}

fn file_write_int(file: &mut File, data: u32){
    let bytes: [u8; 4] = unsafe { transmute(data.to_le()) };
    file.write(&bytes).expect("Error writing file");
}

fn write_person(file: &mut File, person: &Person) {
    file_write_string(file, &person.0, 40);
    file_write_string(file, &person.1, 40);
    file_write_string(file, &person.2, 240);
    file_write_int(file, person.3);
}

fn main() {
    let mut file_input = File::open("pgu.dat").expect("Cannot open file");
    let mut file_output = File::create("pgu.output.dat").expect("Cannot create file");

    let file_length = file_input.metadata().unwrap().len();

    while file_input.seek(SeekFrom::Current(0)).unwrap() < file_length {
        let mut person = read_person(&mut file_input);
        println!("Person: {:?}", person);
        person.3 += 1;
        write_person(&mut file_output, &person);
    }
}
