for c in `find -type f -name Cargo.toml`; do
	pushd `dirname "$c"`
	cargo clippy
	popd
done