#![deny(unsafe_code)]
#![no_main]
#![no_std]

use core::f32::consts::PI;

#[allow(unused_imports)]
use aux15::{entry, iprint, iprintln, prelude::*, switch_hal::OutputSwitch, Direction, I16x3};

// we need this for the atan function
use m::Float;

#[entry]
fn main() -> ! {
    let (leds, mut lsm303dlhc, mut delay, _itm) = aux15::init();
    let mut leds = leds.into_array();

    loop {
        let I16x3 { x, y, .. } = lsm303dlhc.mag().unwrap();

        let theta = (y as f32).atan2(x as f32); // in radians

        let dir = if theta < (-3. * PI / 4.) {
            Direction::North        // between -135 and -180 (smallest possible range)
        } else if theta < (-PI / 2.) {
            Direction::Northwest    // between -135 and -90
        } else if theta < (-1. * PI / 4.) {
            Direction::West         // between -90 and -45
        } else if theta < (0.) {
            Direction::Southwest    // between -45 and -0
        } else if theta < (1. * PI / 4.) {
            Direction::South        // between 0 and 45
        } else if theta < (PI / 2.) {
            Direction::Southeast    // between 45 and 90
        } else if theta < (3. * PI / 4.) {
            Direction::East         // between 90 and 135
        } else if theta < (7. * PI / 8.) {
            Direction::Northeast    // between 135 and 180
        } else {
            Direction::North        // default direction is North
        };

        // turn off all leds
        for i in 0..8 {
            leds[i].off().unwrap();
        }

        // turn on led of selected direction
        leds[dir as usize].on().unwrap();

        delay.delay_ms(50_u8);
    }
}