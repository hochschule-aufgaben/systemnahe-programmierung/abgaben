# Building a LED Compass with STM32 Discovery using Rust
## Introduction
This project details the process of building an LED compass using the STM32 Discovery board programmed in Rust. The compass uses a magnetometer to determine direction and displays it through LEDs.

This is one of the many fun and interesting projects shown in the embedded rust book. In this documentation we will be presenting our solution to this particular exercise, as well as explaining how trivials things in other languages, such as initializing peripherals are done in embedded Rust.

link to the ebook: [embedded rust book](https://docs.rust-embedded.org/discovery/f3discovery/01-background/index.html)
## Requirements
- Hardware:
  - STM32 Discovery board
  - on board Magnetometer (e.g., LSM303DLHC)
  - on board LEDs
- Software:
  - Rust 
  - openOCD
  - gdb
  - cargo

## Setup
prerequisites of project as well as instructions to install them can be found [here](https://docs.rust-embedded.org/discovery/f3discovery/03-setup/linux.html).

## Debugging and Flashing

Debugging is done using openOCD and gdb.
1. execute the following command:
```bash
# start localhost for gdb debugging, this process will block. Keep it open for the whole debug session. 
$ openocd -f interface/stlink-v2-1.cfg -f board/stm32f3discovery.cfg
```
2. execute following command on project directory:
```bash
# we configured it so that this builds and launches gdb for us
cargo run
```

3. once in gdb, the executable can be flashed likeso:
```gdb
# alternative: openocd -f interface/stlink-v2-1.cfg -f board/stm32f3discovery.cfg -c "program <path/to/executable> verify reset exit"
(gdb) load
```

## Main Code

```rust
#![deny(unsafe_code)]
#![no_main]
#![no_std]

use core::f32::consts::PI;

#[allow(unused_imports)]
use aux15::{entry, iprint, iprintln, prelude::*, switch_hal::OutputSwitch, Direction, I16x3};

// we need this for the atan function
use m::Float;

#[entry]
fn main() -> ! {
    let (leds, mut lsm303dlhc, mut delay, _itm) = aux15::init();
    let mut leds = leds.into_array();

    loop {
        let I16x3 { x, y, .. } = lsm303dlhc.mag().unwrap();

        let theta = (y as f32).atan2(x as f32); // in radians

        let dir = if theta < (-3. * PI / 4.) {
            Direction::North        // between -135 and -180 (smallest possible range)
        } else if theta < (-PI / 2.) {
            Direction::Northwest    // between -135 and -90
        } else if theta < (-1. * PI / 4.) {
            Direction::West         // between -90 and -45
        } else if theta < (0.) {
            Direction::Southwest    // between -45 and -0
        } else if theta < (1. * PI / 4.) {
            Direction::South        // between 0 and 45
        } else if theta < (PI / 2.) {
            Direction::Southeast    // between 45 and 90
        } else if theta < (3. * PI / 4.) {
            Direction::East         // between 90 and 135
        } else if theta < (7. * PI / 8.) {
            Direction::Northeast    // between 135 and 180
        } else {
            Direction::North        // default direction is North
        };

        // turn off all leds
        for i in 0..8 {
            leds[i].off().unwrap();
        }

        // turn on led of selected direction
        leds[dir as usize].on().unwrap();

        delay.delay_ms(50_u8);
    }
}
```

### 1.Initialization of Peripherals 
Initialize peripherals using the aux15::init() function. This includes setting up the LED array, the LSM303DLHC magnetometer, a delay provider, and an ITM (Instrumentation Trace Macrocell) for debugging. 

### 2.Magnetometer Data Acquisition
Retrieve the X and Y magnetic field values from the LSM303DLHC magnetometer using lsm303dlhc.mag(). The Z value is ignored.

### 3.Angle Calculation
Calculate the angle theta in radians from the X and Y magnetometer readings. This is done using the atan2 function, which returns the angle between the positive X-axis and the magnetic field vector in the XY plane.

### 4.Direction Determination
The angle theta is then used to categorize the orientation into one of eight possible directions (North, Northwest, West, Southwest, South, Southeast, East, Northeast). This is done by dividing the full circle into eight segments, each corresponding to a specific direction.
![img](img/circle.jpeg)

### 5.LED Control for Direction Indication
All LEDs are first turned off. Then, based on the determined direction, the corresponding LED is turned on. The index of the LED in the array corresponds to the enumerated value of the direction.

## Initialization Code
```rust
//! Initialization code

#![no_std]

#[allow(unused_extern_crates)] // NOTE(allow) bug rust-lang/rust#53964
extern crate panic_itm; // panic handler

pub use cortex_m::{asm::bkpt, iprint, iprintln, peripheral::ITM};
pub use cortex_m_rt::entry;
pub use stm32f3_discovery::{
    leds::Leds,
    lsm303dlhc::I16x3,
    stm32f3xx_hal::{delay::Delay, prelude, stm32::i2c1},
    switch_hal,
};

use stm32f3_discovery::{
    lsm303dlhc,
    stm32f3xx_hal::{
        gpio::gpiob::{PB6, PB7},
        gpio::AF4,
        i2c::I2c,
        prelude::*,
        stm32::{self, I2C1},
    },
};

pub type Lsm303dlhc = lsm303dlhc::Lsm303dlhc<I2c<I2C1, (PB6<AF4>, PB7<AF4>)>>;

/// Cardinal directions. Each one matches one of the user LEDs.
pub enum Direction {
    /// North / LD3
    North,
    /// Northeast / LD5
    Northeast,
    /// East / LD7
    East,
    /// Southeast / LD9
    Southeast,
    /// South / LD10
    South,
    /// Southwest / LD8
    Southwest,
    /// West / LD6
    West,
    /// Northwest / LD4
    Northwest,
}

pub fn init() -> (Leds, Lsm303dlhc, Delay, ITM) {
    let cp = cortex_m::Peripherals::take().unwrap();
    let dp = stm32::Peripherals::take().unwrap();

    let mut flash = dp.FLASH.constrain();
    let mut rcc = dp.RCC.constrain();

    let clocks = rcc.cfgr.freeze(&mut flash.acr);

    let mut gpioe = dp.GPIOE.split(&mut rcc.ahb);
    let leds = Leds::new(
        gpioe.pe8,
        gpioe.pe9,
        gpioe.pe10,
        gpioe.pe11,
        gpioe.pe12,
        gpioe.pe13,
        gpioe.pe14,
        gpioe.pe15,
        &mut gpioe.moder,
        &mut gpioe.otyper,
    );

    let mut gpiob = dp.GPIOB.split(&mut rcc.ahb);
    let scl = gpiob.pb6.into_af4(&mut gpiob.moder, &mut gpiob.afrl);
    let sda = gpiob.pb7.into_af4(&mut gpiob.moder, &mut gpiob.afrl);

    let i2c = I2c::new(dp.I2C1, (scl, sda), 400.khz(), clocks, &mut rcc.apb1);

    let lsm303dlhc = Lsm303dlhc::new(i2c).unwrap();

    let delay = Delay::new(cp.SYST, clocks);

    (leds, lsm303dlhc, delay, cp.ITM)
}

```

### Imports 
```rust
#![no_std]

#[allow(unused_extern_crates)]
extern crate panic_itm;

pub use cortex_m::{asm::bkpt, iprint, iprintln, peripheral::ITM};
.
.
.
```

- #![no_std]: Indicates that the standard library is not used, common in embedded Rust for bare-metal systems.

```rust
use stm32f3_discovery::{
    lsm303dlhc,
    stm32f3xx_hal::{
        gpio::gpiob::{PB6, PB7},
        gpio::AF4,
        i2c::I2c,
        prelude::*,
        stm32::{self, I2C1},
    },
};

pub type Lsm303dlhc = lsm303dlhc::Lsm303dlhc<I2c<I2C1, (PB6<AF4>, PB7<AF4>)>>;
```

- import specific peripherals and GPIO configurations needed for the project, including GPIO pins PB6 and PB7 used for I2C communication for communicating with the Lsm303dlhc.
- the Lsm303dlhc is a type alias is defined for convenience, representing the Lsm303dlhc sensor configured with the I2C interface.

### Direction Enum
```rust
pub enum Direction {
    North,
    Northeast,
    East,
    Southeast,
    South,
    Southwest,
    West,
    Northwest,
}
```
- enumeration representing directions. Each variant of this enum will later correspond to a specific LED on the board.

### Core and Peripherals Access 
```rust
let cp = cortex_m::Peripherals::take().unwrap();
let dp = stm32::Peripherals::take().unwrap();
```
- take ownerships of the core (cortex_m::Peripherals) and device-specific (stm32::Peripherals) peripherals. These will later be used for initializing/configuring other perihpherals.

### clock config
```rust
let mut flash = dp.FLASH.constrain();
let mut rcc = dp.RCC.constrain();
let clocks = rcc.cfgr.freeze(&mut flash.acr);
```

### LED init
```rust
let mut gpioe = dp.GPIOE.split(&mut rcc.ahb);
let leds = Leds::new( ... );
```

### I2C Configuration for LSM303DLHC:
```rust 
let mut gpiob = dp.GPIOB.split(&mut rcc.ahb);
let scl = gpiob.pb6.into_af4(&mut gpiob.moder, &mut gpiob.afrl);
let sda = gpiob.pb7.into_af4(&mut gpiob.moder, &mut gpiob.afrl);
let i2c = I2c::new(dp.I2C1, (scl, sda), 400.khz(), clocks, &mut rcc.apb1);
let lsm303dlhc = Lsm303dlhc::new(i2c).unwrap();
```

## End result

![gif](img/demonstration.gif)

the LED changes to indicate the compass direction as detected by the LSM303DLHC magnetometer.