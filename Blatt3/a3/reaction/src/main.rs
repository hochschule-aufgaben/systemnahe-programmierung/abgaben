use std::thread;
use std::time;
use std::time::Instant;
use std::io;
use std::io::BufRead;

fn main() {
    println!("Another message will appear in a bit, press any key ASAP after that.");

    let delay = (rand::random::<f32>() * 5000.0) as u64;

    thread::sleep(time::Duration::from_millis(delay));

    let stdin = io::stdin();
    let mut iterator = stdin.lock().lines();

    println!("Press the key!");
    let start = Instant::now();

    iterator.next();

    let elapsed = start.elapsed().as_secs_f32();

    println!("That took {elapsed} seconds");
}
