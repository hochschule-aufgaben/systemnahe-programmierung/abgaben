use std::process::exit;
use clap_v3::clap_app;
use std::time;
use std::thread;

fn main() {
    let matches = clap_app!(myapp => 
        (version: "1.0")
        (author: "Daniel Dakhno")
        (@arg EXIT: -e --exit +takes_value +required "Exit-Code <c>")
        (@arg DELAY: -d --delay +takes_value "Delay in Sekunden (default: 0 sec)")
    ).get_matches();

    let exit_code = matches.value_of("EXIT").unwrap_or("0").parse::<i32>().unwrap();
    let delay = matches.value_of("DELAY").unwrap_or("0").parse::<u64>().unwrap();

    thread::sleep(time::Duration::from_secs(delay));

    exit(exit_code);
}
