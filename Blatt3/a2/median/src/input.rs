// von Esteban Borai <estebanborai@gmail.com>

use std::io::{self, Write};

/// Prints a message to `stdout` if provided.
/// And reads a `String` from `stdin` and returns it
pub fn input(msg: Option<&str>) -> String {
    if let Some(msg) = msg {
        print!("{}", msg);

        io::stdout().flush().expect("Unable to print to stdout");
    }

    let mut value = String::new();

    io::stdin()
        .read_line(&mut value)
        .expect("Unable to read from stdin");

    value
}
