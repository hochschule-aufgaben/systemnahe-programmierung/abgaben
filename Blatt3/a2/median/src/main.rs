mod input;

use std::collections::HashMap;

fn parse_int(value: &str) -> i32{
    value
        .trim()
        .parse::<i32>()
        .unwrap_or_else(|_| panic!("{} is not a valid number", value))
}

fn main() {
    let line = input::input(Some("Please enter numbers: "));

    let parts = line.split(' ');

    let numbers = parts.map(parse_int).collect::<Vec<i32>>();   

    let sum: i32 = numbers.iter().sum();

    let median: f32 = (sum as f32) / (numbers.len() as f32);

    println!("median: {median}");

    let mode = numbers[numbers.len() / 2];

    println!("mode: {mode}");

    let mut map: HashMap<i32, i32> = HashMap::new();

    for number in numbers {
        *(map.entry(number).or_insert(0)) += 1;
    }

    let mut max_count = i32::MIN;
    let mut max_index = 0;

    for (number, count) in map {
        if count > max_count {
            max_count = count;
            max_index = number;
        }
    }
    println!("most occurences: {max_index} ({max_count})");
}
