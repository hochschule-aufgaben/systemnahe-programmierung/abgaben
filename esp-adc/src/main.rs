use std::{
    thread::sleep,
    time::Duration,
};
use esp_idf_sys as _;
use esp_idf_hal::{
    peripherals::Peripherals,
    adc::config::Config,
    adc::*
};
use esp_idf_svc::{
    wifi::EspWifi,
    nvs::EspDefaultNvsPartition,
    eventloop::EspSystemEventLoop,
};
use embedded_svc::wifi::{ClientConfiguration, Wifi, Configuration};

fn main(){
    esp_idf_sys::link_patches();//Needed for esp32-rs
    println!("Entered Main function!");
    let peripherals = Peripherals::take().unwrap();
    let sys_loop = EspSystemEventLoop::take().unwrap();
    let nvs = EspDefaultNvsPartition::take().unwrap();

    let mut wifi_driver = EspWifi::new(
        peripherals.modem,
        sys_loop,
        Some(nvs)
    ).unwrap();

    #[cfg(not(esp32))]
    let mut adc = AdcDriver::new(peripherals.adc1, &Config::new().calibration(true))?;

    #[cfg(esp32)]
    let mut adc = AdcDriver::new(peripherals.adc2, &Config::new().calibration(true))?;

    #[cfg(not(esp32))]
    let mut adc_pin: esp_idf_hal::adc::AdcChannelDriver<{ attenuation::DB_11 }, _> =
        AdcChannelDriver::new(peripherals.pins.gpio4)?;

    #[cfg(esp32)]
    let mut adc_pin: esp_idf_hal::adc::AdcChannelDriver<{ attenuation::DB_11 }, _> =
        AdcChannelDriver::new(peripherals.pins.gpio12)?;

    wifi_driver.set_configuration(&Configuration::Client(ClientConfiguration{
        ssid: "EmSys".into(),
        password: "EmSys123".into(),
        ..Default::default()
    })).unwrap();

    wifi_driver.start().unwrap();
    wifi_driver.connect().unwrap();
    while !wifi_driver.is_connected().unwrap(){
        let config = wifi_driver.get_configuration().unwrap();
        println!("Waiting for station {:?}", config);
    }
    println!("Should be connected now");

    let mut adc_old = 0;

    loop{
        sleep(Duration::from_millis(100));
        let adc_current = adc.read(&mut adc_pin)?;
        if adc_current == adc_old {
            continue;
        }
        // send new udp packet
        adc_old = adc_current;
    }

}
