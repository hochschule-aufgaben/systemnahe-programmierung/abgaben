use rand::prelude::*;
use std::fs;

fn main() {

    let contents: String = fs::read_to_string("randomchoicelines.txt")
        .expect("unable to read from file");
    // some fancy sh!t
    let lines: Vec<&str> =  contents.split('\n').filter(|line| !line.is_empty()).collect();
    
    // generate a value between 
    let rng = rand::thread_rng().gen_range(0..(lines.len() - 1));
    println!("{}", lines[rng])
}