extern crate subprocess;
use subprocess::{ Popen, PopenConfig};
use std::time::Instant;

fn main()
{
    // call subprocess, pipe parent stdin and stdout to child's with Redirection::None
    // test for "-e <Echo stdin -> stdout"
    let mut subprocess = Popen::create(&["../child/target/debug/child", "-e"], PopenConfig {
        stdin: subprocess::Redirection::Pipe, stdout: subprocess::Redirection::Pipe, ..Default::default()
    }).unwrap();

    // write something to child's stdin 
    let message = "abcd.";

    // instanciate communicator object
    let mut communicator = subprocess.communicate_start(Some(message.as_bytes().to_vec()));
    let result = communicator.read_string().unwrap();

    if result.0.unwrap() != "abcd" {
        panic!("Process did not echo its input");
    }

    println!("subprocess successfully echoed input");

    // write something to start process

    let mut exit_code = subprocess.wait().unwrap();
    if exit_code.success(){
        println!("subprocess exited sucessfully with 0");
    }else{
        panic!("subprocess exited with code {:?}", exit_code);
    }

    // test for -r (exit) with exit code 50 and -d (delay) for 2 seconds
    subprocess = Popen::create(&["../child/target/debug/child", "-r", "50", "-d", "2"], PopenConfig {
        stdin: subprocess::Redirection::Pipe, stdout: subprocess::Redirection::Pipe, ..Default::default()
    }).unwrap();

    // start recording 
    let start_time = Instant::now();
    
    exit_code = subprocess.wait().unwrap();

    let end_time = Instant::now();
    if matches!(exit_code, subprocess::ExitStatus::Exited(50)) {   
        println!("subprocess exited sucessfully with 50");
        println!("total time delay: {:?}", end_time.duration_since(start_time));
    }else{
        panic!("subprocess exited with code {:?}", exit_code);    
    }
}
    


    
 