use std::io::{self, Write};
use termion::event::Key;
use termion::input::TermRead;
use clap::clap_app;
use std::process::exit;
use std::time;
use std::thread;

fn main()
{
    let matches = clap_app!(myapp => 
        (version: "1.0")
        (author: "Chee Heng Yong")
        (@arg EXIT: -r --return +takes_value "Exit-Code <c>")
        (@arg DELAY: -d --delay +takes_value "Delay in Sekunden (default: 0 sec)")
        (@arg ECHO: -e --echo "Echo stdin to stdout")
    ).get_matches();

    if matches.is_present("EXIT") {
        let exit_code = matches.value_of("EXIT").unwrap_or("0").parse::<i32>().unwrap();
        let delay = matches.value_of("DELAY").unwrap_or("0").parse::<u64>().unwrap();
    
        thread::sleep(time::Duration::from_secs(delay));
    
        exit(exit_code);
    }

    if !matches.is_present("ECHO") {
        println!("Either -r or -e is needed");
        exit(1);
    }
    
    let stdin = io::stdin();
    let mut stdout = io::stdout();

    // detect keystrokes 
    for c in stdin.keys()
    {
        let _ = match c.unwrap() {
            Key::Char('.') => break,
            Key::Char(c) => write!(stdout, "{}", c),
            _ => Ok(()),
        };
        
        stdout.flush().unwrap();
    }
   
}
