# Blatt 9

## 1.
`f.read_to_string().unwrap_or("root")`

## 2.
```
fn main() -> Result<(), &'static str> {
        let s = vec!["apple", "mango", "banana"];
        let fourth = s.get(4).expect("I got only 3 fruits")?;
        Ok(())
}
```

## 3.
```
use std::fs::File;
use std::io::ErrorKind;

fn main() {
   let greeting_file = File::open("hallo.txt").unwrap_or_else(|error| {
        match error.kind() {
           ErrorKind::NotFound => match File::create("hallo.txt") {
               Ok(fc) => fc,
               Err(e) => panic!("Problem beim Erstellen der Datei: {:?}", e),
           },
           other_error => {
               panic!("Problem beim Öffnen der Datei: {:?}", other_error)
           }
       }
   });
}
```

## 4.
```
fn double_arg(mut argv: env::Args) -> Result<i32, String> {                     
    argv.nth(1)                                                                 
        .ok_or("Please give at least one argument".to_owned())                  
        .and_then(|arg| arg.parse::<i32>().map_err(|err| err.to_string()))      
        .map(|i| i * 2)                                                         
}    

```

Dieser code versucht, den ersten parameter zu einem int zu konvertieren.
Sollte dieser nicht vorhanden sein, wird ein Fehler ausgegeben.
Sollte die Konvertierung fehlschlagen, wird der entsprechende Fehler ausgegeben.

## 5.
```
use std::io;

fn f5(arg: i32) -> Result<i32, io::Error>{
    if arg == 0 {
        Err(io::Error::new(io::ErrorKind::Other, "Parameter darf nicht 0 sein"))?;
    }
    if arg == 10 {
        Err(io::Error::new(io::ErrorKind::Other, "Parameter darf nicht 10 sein"))?;
    }
    Ok(arg * 2)
}

fn main(){
    println!("Result for 0 is {}", f5(0).expect("Error for arg 0"));
}
```

## 6.
```
fn f6(arg: i32) -> Result<i32, Box<dyn std::error::Error>>{
    if arg == 0 {
        return Err(Box::new(Error::new(ErrorKind::Other)));
    }
    Ok(arg * 2)
}
```