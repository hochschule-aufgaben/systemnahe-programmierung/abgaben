use std::str::FromStr;
use std::num::IntErrorKind::InvalidDigit;
use std::result::Result;

// this function reads in a CSV-like format with ";" as line seperator and "," as value seperator
fn _parse_list(input: &str) -> Result<Vec<Vec<u32>>, <u32 as FromStr>::Err> {    
    input                                                                       
        .split(';')                                                             
        .map(|s| s.trim())                                                      
        .filter(|s| !s.is_empty())                                              
        .map(|s| {                                                              
            s.split(',')                                                        
            .map(|s| s.trim())                                              
            .filter(|s| !s.is_empty())                                      
            .map(|s| s.parse())                                             
            .collect()                                                      
        })                                                                      
        .collect()                                                              
}   


fn main() {
    println!("Hello, world!");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_single_line() {
        let result = _parse_list("1,2,3");
        assert_eq!(Result::Ok(vec!(vec!(1, 2, 3))), result);
    }

    #[test]
    fn test_multi_line() {
        let result = _parse_list("1,2,3;4,5,6;7,8,9");
        assert_eq!(Result::Ok(vec!(vec!(1, 2, 3), vec!(4, 5, 6), vec!(7, 8, 9))), result);
    }

    #[test]
    fn test_parse_error() {
        let result = _parse_list("1,2,a;4,5,6;7,8,9").unwrap_err();
        assert_eq!(InvalidDigit, result.kind().clone());
    }
}
