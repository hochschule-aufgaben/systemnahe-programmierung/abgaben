use std::collections::HashMap;
use std::hash::Hash;

struct Cacher<'a, A, R>
{
    calculation: &'a dyn Fn(A) -> R,
    cache: HashMap<A, R>
}

impl<A, R> Cacher<'_, A, R>
where A: Eq, A: Hash, A: Copy, R: Copy
{
    fn new(calculation: &dyn Fn(A) -> R) -> Cacher<A, R> {
        Cacher {
            calculation,
            cache: HashMap::new()
        }
    }

    fn value(&mut self, arg: A) -> R {
        *self.cache.entry(arg).or_insert((self.calculation)(arg))
    }
}

 #[cfg(test)]
 mod tests {
    use super::*;

    #[test]
    fn call_with_different_values() {
        let mut c = Cacher::<i32, i32>::new(&|a| a);

        let v1 = c.value(1);
        let v2 = c.value(2);
        let v22 = c.value(22);

        assert_eq!(v1, 1);
        assert_eq!(v2, 2);
        assert_eq!(v22, 22);
    }
}    