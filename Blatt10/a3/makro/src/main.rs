use std::collections::HashMap;

macro_rules! hash_map {
    ($($key: expr => $value: expr),*) => {
        {
            let mut map = HashMap::new();
            $(
                map.insert($key, $value);
            )*
            map
        }
    }
}

fn main() {
    let ages = hash_map! { "Maria" => 26, "Peter" => 32 };
    println!("{:#?}", ages);
}